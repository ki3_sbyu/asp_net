﻿$('#search').on('change', function () {
    $.ajax(
        {
            data: { search: $('#search').val() },
            type: "GET",
            url: "/Car/CarSearch",
            dataType: "html",
            success: function (data) {
                $('#content').html(data);
            }
        })
})