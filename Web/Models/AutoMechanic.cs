﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MySqlApp.Models
{
    public class AutoMechanic
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [DisplayName("Full name")]
        public string FullName { get; set; }

        [Required]
        public int Salary { get; set; }

        [Required]
        public int Category { get; set; }

        [Required]
        [DisplayName("Personnel number")]
        public int PersonnelNumber { get; set; }

        public virtual ICollection<Work> Works { get; set; }
    }
}