﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MySqlApp.Models
{
    public class Car
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Brand { get; set; }

        [Required]
        [StringLength(20)]
        public string Number { get; set; }

        [Column("FullNameOfTheOwner")]
        [Required, StringLength(100)]
        [DisplayName("Full name of the owner")]
        public string OwnerFullName { get; set; }

        [Required]
        [Range(1960, 2019)]
        [Display(Name = "Year of manufacture")]
        public int YearOfManufacture { get; set; }

        public virtual ICollection<Work> Works { get; set; }
    }
}