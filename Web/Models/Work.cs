﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MySqlApp.Models
{
    public class Work
    {
        public int Id { get; set; }

        [Required]
        public int Price { get; set; }

        [Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Date of receipt")]
        [Column(TypeName = "date")]
        public DateTime DateOfReceipt { get; set; }

        [StringLength(100)]
        [DataType(DataType.MultilineText)]
        public string Category { get; set; }

        [Required]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Planned end date")]
        [Column(TypeName = "date")]
        public DateTime PlannedEndDate { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Real end date")]
        [Column(TypeName = "date")]
        public DateTime? RealEndDate { get; set; }

        public int? CarId { get; set; }

        public int? AutoMechanicId { get; set; }

        public virtual Car Car { get; set; }

        public virtual AutoMechanic AutoMechanic { get; set; }
    }
}