﻿using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MySqlApp.Models
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class AutoRepairShopContext : DbContext
    {
        public AutoRepairShopContext() : base("default")
        {
        }

        public DbSet<AutoMechanic> AutoMechanics { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Work> Works { get; set; }
    }
}