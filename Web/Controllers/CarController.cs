﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MySqlApp.Models;

namespace Web.Controllers
{
    public class CarController : Controller
    {
        private readonly AutoRepairShopContext db = new AutoRepairShopContext();

        [HttpGet]
        public ActionResult Index()
        {
            return View(db.Cars.ToList());
        }

        [HttpGet]
        public ActionResult CarSearch(string search)
        {
            var cars = db.Cars.Where(c => c.Brand.ToLower().Contains(search.ToLower())).ToList();
            return PartialView("Search", cars);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return HttpNotFound();

            var car = db.Cars.Find(id);
            if (car == null)
                return HttpNotFound();

            return View(car);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Car car)
        {
            if (!ModelState.IsValid)
                return View(car);

            db.Cars.Add(car);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var car = db.Cars.Find(id);
            if (car == null)
                return HttpNotFound();

            return View(car);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Car car)
        {
            if (ModelState.IsValid)
            {
                db.Entry(car).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(car);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var car = db.Cars.Find(id);
            if (car == null)
                return HttpNotFound();

            return View(car);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var car = db.Cars.Find(id);
            db.Cars.Remove(car);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
