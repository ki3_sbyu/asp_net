﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MySqlApp.Models;

namespace Web.Controllers
{
    public class MechanicController : Controller
    {
        private readonly AutoRepairShopContext db = new AutoRepairShopContext();

        [HttpGet]
        public ActionResult Index()
        {
            return View(db.AutoMechanics.ToList());
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return HttpNotFound();

            var autoMechanic = db.AutoMechanics.Find(id);
            if (autoMechanic == null)
                return HttpNotFound();

            return View(autoMechanic);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AutoMechanic autoMechanic)
        {
            if (ModelState.IsValid)
            {
                db.AutoMechanics.Add(autoMechanic);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(autoMechanic);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var autoMechanic = db.AutoMechanics.Find(id);
            if (autoMechanic == null)
                return HttpNotFound();

            return View(autoMechanic);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FullName,Salary,Category,PersonnelNumber")] AutoMechanic autoMechanic)
        {
            if (ModelState.IsValid)
            {
                db.Entry(autoMechanic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(autoMechanic);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var autoMechanic = db.AutoMechanics.Find(id);
            if (autoMechanic == null)
                return HttpNotFound();

            return View(autoMechanic);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var autoMechanic = db.AutoMechanics.Find(id);
            db.AutoMechanics.Remove(autoMechanic);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
