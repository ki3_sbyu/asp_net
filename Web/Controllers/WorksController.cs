﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MySqlApp.Models;

namespace Web.Controllers
{
    public class WorksController : Controller
    {
        private readonly AutoRepairShopContext db = new AutoRepairShopContext();

        [HttpGet]
        public ActionResult Index()
        {
            var works = db.Works.Include(w => w.AutoMechanic).Include(w => w.Car).OrderBy(w => w.DateOfReceipt);
            return View(works.ToList());
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return HttpNotFound();

            var work = db.Works.Find(id);
            if (work == null)
                return HttpNotFound();

            return View(work);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.AutoMechanicId = new SelectList(db.AutoMechanics, "Id", "FullName");
            ViewBag.CarId = new SelectList(db.Cars, "Id", "Brand");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Work work)
        {
            if (ModelState.IsValid)
            {
                db.Works.Add(work);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AutoMechanicId = new SelectList(db.AutoMechanics, "Id", "FullName", work.AutoMechanic.Id);
            ViewBag.CarId = new SelectList(db.Cars, "Id", "Brand", work.Car.Id);
            return View(work);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var work = db.Works.Find(id);
            if (work == null)
                return HttpNotFound();

            ViewBag.AutoMechanicId = new SelectList(db.AutoMechanics, "Id", "FullName", work.AutoMechanic.Id);
            ViewBag.CarId = new SelectList(db.Cars, "Id", "Brand", work.Car.Id);
            return View(work);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Work work)
        {
            if (ModelState.IsValid)
            {
                db.Entry(work).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AutoMechanicId = new SelectList(db.AutoMechanics, "Id", "FullName", work.AutoMechanic.Id);
            ViewBag.CarId = new SelectList(db.Cars, "Id", "Brand", work.Car.Id);
            return View(work);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var work = db.Works.Find(id);
            if (work == null)
                return HttpNotFound();

            return View(work);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var work = db.Works.Find(id);
            db.Works.Remove(work);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
